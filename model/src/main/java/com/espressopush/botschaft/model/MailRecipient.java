package com.espressopush.botschaft.model;

import javax.persistence.*;

/**
 * Created by kpasko on 11.09.2014.
 */
@Entity
public class MailRecipient {
    @Id
    private Long id;

    @Column
    private String email;

    @Column
    private String barcodeNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBarcodeNumber() {
        return barcodeNumber;
    }

    public void setBarcodeNumber(String barcodeNumber) {
        this.barcodeNumber = barcodeNumber;
    }
}
