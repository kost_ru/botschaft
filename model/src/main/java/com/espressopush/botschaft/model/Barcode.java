package com.espressopush.botschaft.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by konstantinpasko on 07.09.14.
 */
@Entity
public class Barcode {
    @Id
    @Column
    private String barNumber;
    @Column
    private String docs;
    @Column
    @Temporal(TemporalType.DATE)
    private Date untilDate;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    public Barcode() {
        this.createDate = new Date();
    }

    public Barcode(String barNumber, String docs, Date untilDate) {
        this.barNumber = barNumber;
        this.docs = docs;
        this.untilDate = untilDate;
        this.createDate = new Date();
    }

    public String getBarNumber() {
        return barNumber;
    }

    public void setBarNumber(String barNumber) {
        this.barNumber = barNumber;
    }

    public String getDocs() {
        return docs;
    }

    public void setDocs(String docs) {
        this.docs = docs;
    }

    public Date getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(Date untilDate) {
        this.untilDate = untilDate;
    }
    @Override
    public String toString() {
        return "Application{" +
                "barNumber='" + barNumber + '\'' +
                ", docs=" + docs +
                ", untilDate=" + untilDate +
                ", publishedDate=" + createDate +
                '}';
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Barcode barcode = (Barcode) o;

        if (barNumber == null ^ barcode.barNumber == null) return false;
        if (barNumber != null && barcode.barNumber != null) {
            if (!barNumber.equals(barcode.barNumber)) return false;
        }
        if (docs == null ^ barcode.docs == null) return false;
        if (docs != null && barcode.docs != null) {
            if (!docs.equals(barcode.docs)) return false;
        }
        if (untilDate == null ^ barcode.untilDate == null) return false;
        if (untilDate != null && barcode.untilDate != null) {
            if (!untilDate.equals(barcode.untilDate)) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = barNumber.hashCode();
        result = 31 * result + docs.hashCode();
        result = 31 * result + untilDate.hashCode();
        return result;
    }
}
