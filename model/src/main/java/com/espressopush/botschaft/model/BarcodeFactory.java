package com.espressopush.botschaft.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by konstantinpasko on 08.09.14.
 */
public class BarcodeFactory {

    private final static SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    public static Barcode createBarcode(String str) {
        String inputStr = str.trim();
        Barcode barcode = new Barcode();
        barcode.setBarNumber(inputStr.substring(0, inputStr.indexOf(" ")));
        String docsStr = inputStr.substring(inputStr.indexOf(" ") + 1, inputStr.lastIndexOf(" "));
        String dateStr = inputStr.substring(inputStr.lastIndexOf(" ") + 1, inputStr.length());
        barcode.setDocs(docsStr.trim());
        try {
            barcode.setUntilDate(formatter.parse(dateStr));
        } catch (ParseException e) {
            barcode.setUntilDate(new Date());
        }
        return barcode;
    }
}
