package com.espressopush.botschaft.model;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by konstantinpasko on 08.09.14.
 */
public class BarcodeTest {

    private final static SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
    private final static String barcodeText = "2957628 RP, WS, KV, Nachweis zur Immatrikulation 15.11.2014 ";

    @Test
    public void testFactory() throws ParseException {
        Barcode barcode = BarcodeFactory.createBarcode(barcodeText);
        Barcode barcodeObj = new Barcode("2957628", "RP, WS, KV, Nachweis zur Immatrikulation", formatter.parse("15.11.2014 "));
        assertEquals(barcodeObj, barcode);
    }
}
