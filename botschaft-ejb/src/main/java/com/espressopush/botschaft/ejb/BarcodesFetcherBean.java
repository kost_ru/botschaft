package com.espressopush.botschaft.ejb;

import com.espressopush.botschaft.html.util.HTMLUtils;
import com.espressopush.botschaft.model.Barcode;
import com.espressopush.botschaft.model.MailRecipient;
import com.espressopush.botschaft.pdf.util.PDFUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cinimex.switfcorp.mailservice.EmailSenderBean;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.net.URL;
import java.util.List;

/**
 * Created by konstantinpasko on 10.09.14.
 */
@Stateless(name = "BarcodesFetcherEJB")
public class BarcodesFetcherBean {

    private static final Logger logger = LoggerFactory.getLogger(BarcodesFetcherBean.class);

    private static final String templateMsg = "Hello, your %s is ready";
    private static final String subject = "German visa";

    @EJB
    BarcodeServiceBean barcodeServiceBean;

    @EJB
    EmailSenderBean emailSenderBean;

    @EJB
    MailRecipientServiceBean mailRecipientServiceBean;

    @Resource
    private String mainUrl;
    @Resource
    private String url;

    public BarcodesFetcherBean() {
    }

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public void processBarcodes() {
        logger.info("begin processBarcodes");
        String page = HTMLUtils.getPage(url);
        if (page != null && !page.isEmpty()) {
            String pdfLink = HTMLUtils.getPDFLink(page);
            if (pdfLink != null && !pdfLink.isEmpty()) {
                URL href = HTMLUtils.getHREF(mainUrl, pdfLink);
                if (href != null && !pdfLink.isEmpty()) {
                    logger.info("link to pdf " + href.toString());
                    String text = PDFUtils.getPdfText(href);
                    if (text != null && !text.isEmpty()) {
                        List<Barcode> barcodes = PDFUtils.getBarcodesList(text);
                        logger.info(barcodes.size() + " barcodes were got");
                        barcodes = barcodeServiceBean.saveAndFilterBarcodeList(barcodes);
                        logger.info(barcodes.size() + " barcodes were new");
                        for (Barcode barcode : barcodes) {
                            logger.info(barcode.getBarNumber() + " is lucky");
                            List<MailRecipient> mailRecipients = mailRecipientServiceBean.getMailByBarcode(barcode);
                            if (mailRecipients != null) {
                                for (MailRecipient mailRecipient : mailRecipients) {
                                    emailSenderBean.send(mailRecipient.getEmail(), subject, String.format(templateMsg, barcodes.get(0).toString()));
                                    logger.info("Letter was sent to " + mailRecipient.getEmail());
                                }
                            }
                        }
                    } else {
                        logger.error("PDF Text wasn't got");
                    }
                } else {
                    logger.error("HREF wasn't got");
                }
            } else {
                logger.error("PDFLink wasn't got");
            }
        } else {
            logger.error("Page wasn't got");
        }
        logger.info("end processBarcodes");
    }
}
