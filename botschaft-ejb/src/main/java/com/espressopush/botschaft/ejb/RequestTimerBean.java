package com.espressopush.botschaft.ejb;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;

/**
 * Created by konstantinpasko on 10.09.14.
 */
@Startup
@Singleton(name = "RequestTimerEJB")
public class RequestTimerBean {

    @Resource
    private Long intervalInHour;

    private Long intervalCoef = 1000L * 60 * 60;

    private static final Logger logger = LoggerFactory.getLogger(RequestTimerBean.class);

    @Resource
    TimerService timerService;

    @EJB
    private BarcodesFetcherBean barcodesFetcherBean;

    @PostConstruct
    public void init() {
        System.out.println("init");
        timerService.createIntervalTimer(0L, intervalInHour * intervalCoef, new TimerConfig(null, false));
    }

    @Timeout
    public void process(Timer timer) {
        logger.info("begin process");
        barcodesFetcherBean.processBarcodes();
        logger.info("end process");
    }

    public RequestTimerBean() {
    }
}
