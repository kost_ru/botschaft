package com.espressopush.botschaft.ejb;

import com.espressopush.botschaft.model.Barcode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.DuplicateKeyException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by konstantinpasko on 11.09.14.
 */
@Stateless(name = "BarcodeServiceEJB")
public class BarcodeServiceBean {
    private static final Logger logger = LoggerFactory.getLogger(BarcodeServiceBean.class);

    @PersistenceContext
    private EntityManager em;

    public BarcodeServiceBean() {
    }

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public void saveBarcode(Barcode barcode) {
        em.persist(barcode);
    }

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public List<Barcode> saveAndFilterBarcodeList(List<Barcode> barcodeList) {
        em.getEntityManagerFactory().getCache().evictAll();
        List<Barcode> result = new ArrayList<Barcode>();
        for (Barcode barcode: barcodeList) {
            if (em.find(Barcode.class, barcode.getBarNumber()) == null) {
                try {
                    em.persist(barcode);
                } catch (Exception ex) {
                    logger.error("Error was occured while saving barcode", ex);
                }
                result.add(barcode);
            }
        }
        em.flush();
        return result;
    }
}
