package com.espressopush.botschaft.ejb;

import com.espressopush.botschaft.model.Barcode;
import com.espressopush.botschaft.model.MailRecipient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by kpasko on 11.09.2014.
 */

@Stateless(name = "MailRecipientServicEJB")
public class MailRecipientServiceBean {

    private static final Logger logger = LoggerFactory.getLogger(MailRecipientServiceBean.class);

    @PersistenceContext
    private EntityManager em;

    public MailRecipientServiceBean() {
    }

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public List<MailRecipient> getMailByBarcode(Barcode barcode) {
        Query q = em.createQuery("select t from MailRecipient t where t.barcodeNumber = :arg1");
        q.setParameter("arg1", barcode.getBarNumber());
        try {
            return (List<MailRecipient>) q.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
