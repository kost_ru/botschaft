package ru.cinimex.switfcorp.mailservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

@Stateless(name = "EmailSenderEJB")
public class EmailSenderBean {
    private static final String ENCODING = "UTF-8";
    private static final String CONTENT_TRANSFER_ENCODING = "base64";
    private static Logger log = LoggerFactory.getLogger(EmailSenderBean.class);

    @Resource(name="mail/botschaft")
    private Session mailSession;

    public EmailSenderBean() {
    }

    public void send(
            String toEmail,
            String subject,
            String bodyText)
            {

        if (this.mailSession == null) {
            String msg = "Mail session is null";

            log.error(msg);
        }


        MimeMessage message = new MimeMessage(this.mailSession);

        try {
            message.addHeader("Content-Transfer-Encoding", CONTENT_TRANSFER_ENCODING);

            message.addRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(toEmail.toUpperCase()));

            message.setSubject(subject, ENCODING);
            message.setContent(bodyText, "text/plain;charset=\"" + ENCODING + "\"");

            Transport.send(message);
        } catch (AddressException e) {
            String msg = "Error occured while parsing email address. Email: " + toEmail;

            log.error(msg, e);
        } catch (MessagingException e) {
            String msg = "Error occured while trying to send email";

            log.error(msg, e);
        }
    }


    public void send(
            String fromEmail,
            String toEmail,
            String subject,
            String bodyText)
            throws Exception {

        if (this.mailSession == null) {
            String msg = "Mail session is null";

            log.error(msg);
            throw new Exception(msg);
        }


        MimeMessage message = new MimeMessage(this.mailSession);

        try {
            message.addHeader("Content-Transfer-Encoding", CONTENT_TRANSFER_ENCODING);

            if (fromEmail != null) {
                message.addFrom(InternetAddress.parse(fromEmail));
            }
            message.addRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(toEmail.toUpperCase()));

            message.setSubject(subject, ENCODING);
            message.setContent(bodyText, "text/plain;charset=\"" + ENCODING + "\"");

            Transport.send(message);
        } catch (AddressException e) {
            String msg = "Error occured while parsing email address. Email: " + toEmail;

            log.error(msg, e);
            throw new Exception(msg, e);
        } catch (MessagingException e) {
            String msg = "Error occured while trying to send email";

            log.error(msg, e);
            throw new Exception(msg, e);
        }
    }


    public void send(
            String fromEmail,
            String toEmail,
            String subject,
            String bodyText,
            DataSource[] attachments)
            throws Exception {

        if (this.mailSession == null) {
            String msg = "Mail session is null";

            log.error(msg);
            throw new Exception(msg);
        }


        MimeMessage message = new MimeMessage(this.mailSession);

        try {
            message.addHeader("Content-Transfer-Encoding", CONTENT_TRANSFER_ENCODING);

            if (fromEmail != null) {
                message.addFrom(InternetAddress.parse(fromEmail));
            }
            message.addRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(toEmail));

            message.setSubject(subject, ENCODING);

            MimeMultipart content = new MimeMultipart();

            // main text part
            MimeBodyPart bodyPart = new MimeBodyPart();
            bodyPart.setContent(bodyText, "text/plain;charset=\"" + ENCODING + "\"");
            content.addBodyPart(bodyPart);

            // attachment parts
            if (attachments != null && attachments.length > 0) {
                for (DataSource attachment : attachments) {
                    MimeBodyPart attachPart = new MimeBodyPart();

                    attachPart.setDataHandler(new DataHandler(attachment));
                    if (attachment.getName() != null) {
                        attachPart.setFileName(
                                MimeUtility.encodeText(
                                        attachment.getName(),
                                        ENCODING,
                                        null));
                    }

                    content.addBodyPart(attachPart);
                }
            }

            message.setContent(content);

            Transport.send(message);
        } catch (AddressException e) {
            String msg =
                    "Error occured while parsing email address. " +
                            "ToEmail: " + toEmail + ", FromEmail: " + fromEmail;

            log.error(msg, e);

            throw new Exception(msg, e);
        } catch (MessagingException e) {
            String msg = "Error occured while trying to send email";

            log.error(msg, e);

            throw new Exception(msg, e);
        } catch (UnsupportedEncodingException e) {
            String msg = "Error occured while trying to encode attachment filename";

            log.error(msg, e);

            throw new Exception(msg, e);
        }
    }


    public void send(
            String fromEmail,
            String toEmail,
            String subject,
            DataSource body,
            DataSource[] attachments)
            throws Exception {

        if (this.mailSession == null) {
            String msg = "Mail session is null";

            log.error(msg);
            throw new Exception(msg);
        }


        MimeMessage message = new MimeMessage(this.mailSession);

        try {
            message.addHeader("Content-Transfer-Encoding", CONTENT_TRANSFER_ENCODING);

            if (fromEmail != null) {
                message.addFrom(InternetAddress.parse(fromEmail));
            }
            message.addRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(toEmail));

            message.setSubject(subject, ENCODING);

            MimeMultipart content = new MimeMultipart();

            // main text part
            MimeBodyPart bodyPart = new MimeBodyPart();
            bodyPart.setDataHandler(new DataHandler(body));
            content.addBodyPart(bodyPart);

            // attachment parts
            if (attachments != null && attachments.length > 0) {
                for (DataSource attachment : attachments) {
                    MimeBodyPart attachPart = new MimeBodyPart();

                    attachPart.setDataHandler(new DataHandler(attachment));
                    if (attachment.getName() != null) {
                        attachPart.setFileName(
                                MimeUtility.encodeText(
                                        attachment.getName(),
                                        ENCODING,
                                        null));
                    }

                    content.addBodyPart(attachPart);
                }
            }

            message.setContent(content);

            Transport.send(message);
        } catch (AddressException e) {
            String msg =
                    "Error occured while parsing email address. " +
                            "ToEmail: " + toEmail + ", FromEmail: " + fromEmail;

            log.error(msg, e);

            throw new Exception(msg, e);
        } catch (MessagingException e) {
            String msg = "Error occured while trying to send email";

            log.error(msg, e);

            throw new Exception(msg, e);
        } catch (UnsupportedEncodingException e) {
            String msg = "Error occured while trying to encode attachment filename";

            log.error(msg, e);

            throw new Exception(msg, e);
        }
    }

}