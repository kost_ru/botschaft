package com.espressopush.botschaft.pdf.util;

import com.espressopush.botschaft.model.Barcode;
import com.espressopush.botschaft.model.BarcodeFactory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by konstantinpasko on 07.09.14.
 */
public class PDFUtils {

    static final String regex = "\\d{7}\\s{1}.*\\d{2}\\.\\d{2}\\.\\d{4}";

    public static String getPdfText(URL url) {
        PDDocument pdf = null;
        try {
            pdf = PDDocument.load(url);
            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setStartPage(3);
            return stripper.getText(pdf);

        } catch (Exception e) {
            return null;
        } finally {
            if (pdf != null) {
                try {
                    pdf.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static List<String> getBarcodes(String text) {
        List<String> barcodes = new ArrayList<String>();
        Pattern pattern = Pattern.compile("\\d{7}\\s{1}.+?\\d{2}\\.\\d{2}\\.\\d{4}", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            barcodes.add(matcher.group(0));
        }
        return barcodes;
    }

    public static List<Barcode> getBarcodesList(String text) {
        List<Barcode> result = new ArrayList<Barcode>();
        for (String barcode : getBarcodes(text)) {
            result.add(BarcodeFactory.createBarcode(barcode));
        }
        return result;
    }



}
