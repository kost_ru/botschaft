package com.espressopush.botschaft.html.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by konstantinpasko on 09.09.14.
 */
public class HTMLUtils {

    private static final String linkRegex = "(?i)<a([^>]+)>(.+?)Entschiedene(.+?)</a>";
    private static final String hrefRegex = "href=\"(.*?)\"";

    public static String getPage(String urlStr) {
        try {
            HttpGet request = new HttpGet(urlStr);
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return EntityUtils.toString(entity);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getPDFLink(String text) {
        Pattern pattern = Pattern.compile(HTMLUtils.linkRegex);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            return null;
        }
    }

    public static URL getHREF(String mainUrl, String link) {
        Pattern pattern = Pattern.compile(HTMLUtils.hrefRegex);
        Matcher matcher = pattern.matcher(link);
        if (matcher.find()) {
            try {
                return new URL(mainUrl+ matcher.group(1));
            } catch (MalformedURLException e) {
                return null;
            }
        } else {
            return null;
        }
    }
}
