package com.espressopush.botschaft.html.util;

import org.junit.Test;

import java.io.InputStream;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * Created by konstantinpasko on 09.09.14.
 */
public class HTMLUtilsTest {

    public final static String url = "http://www.germania.diplo.de/Vertretung/russland/ru/02-mosk/1-visa/3-merkblaetter/nationale-visa/0-nationale-visa.html";
    public final static String mainUrl = "http://www.germania.diplo.de";

    @Test
    public void testGetPage() {
        assertNotNull(HTMLUtils.getPage(url));
    }

    @Test
    public void testGetLink() {
        String page = HTMLUtils.getPage(url);
        assertNotNull(HTMLUtils.getPDFLink(page));
    }

    @Test
    public void testGetHREF() {
        String page = HTMLUtils.getPage(url);
        String link = HTMLUtils.getPDFLink(page);
        assertNotNull(HTMLUtils.getHREF(mainUrl, link));
    }

}
