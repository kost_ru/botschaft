package com.espressopush.botschaft.pdf.util;

import com.espressopush.botschaft.html.util.HTMLUtils;
import com.espressopush.botschaft.html.util.HTMLUtilsTest;
import com.espressopush.botschaft.model.Barcode;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

/**
 * Created by konstantinpasko on 07.09.14.
 */
public class PDFBoxTest {

    private static final String goodStaing = "2777801 RP, WS, KV 31.10.2014 \n";
    private static final String realExample = "3003288 RP, WS, KV 27.10.2014 \n" +
            "3003290 RP, WS, KV, \n" +
            "Immatrikulationbescheinigung \n" +
            "17.10.2014 \n" +
            "3004233 RP, WS, KV 20.10.2014 ";
    private static final String textExample = "Документы, которые необходимо \n" +
            "иметь при себе \n" +
            "Visum spätestens abholen bis/  \n" +
            "Забрать визу не позднее \n" +
            "2777801 RP, WS, KV 31.10.2014 \n" +
            "2777804 RP, WS, KV 31.10.2014 \n" +
            "2777805 RP, WS, KV 31.10.2014 \n" +
            "2777806 RP, WS, KV 31.10.2014 \n" +
            "2777807 RP, WS, KV 31.10.2014 \n" +
            "2832242 RP, WS, KV, TER 21.11.2014 \n" +
            "2952746 RP, WS, KV 12.09.2014 \n" +
            "2953872 RP, WS, KV, aktuelles PF 10.11.2014 \n" +
            "  Adresse der Visastelle: Tel.: E-Mail:  Termine für die Visumbeantragung: \n" +
            " Leninski Prospekt 95 a +7-4 95-9 33 43 11 d-visa@mosk.diplo.de (nur über Firma VFS Global, normale Telefongebühren) \n" +
            " 119 313  M o s k a u Fax: +49-30 18-1 76 71 28 Internet: Tel.: +7-4 99-6 81 13 65  \n" +
            " Russische Föderation +7-4 95-9 36 21 43 www.germania.diplo.de Tel.: +7-4 99-4 26 03 25 \n" +
            " \n" +
            "2953874 RP, WS, KV 10.11.2014 \n" +
            "2956487 RP, WS, KV 31.10.2014 \n" +
            "2957628 RP, WS, KV, SprachZ (A1) 15.11.2014 ";
    private static final int countGroups = 11;


    @Test
    public void testCreate() throws IOException {
        String page = HTMLUtils.getPage(HTMLUtilsTest.url);
        String pdfLink = HTMLUtils.getPDFLink(page);
        URL href = HTMLUtils.getHREF(HTMLUtilsTest.mainUrl, pdfLink);
        String text = PDFUtils.getPdfText(href);
        assertNotNull(text);
        assertFalse(text.isEmpty());
        List<Barcode> barcodes = PDFUtils.getBarcodesList(text);
        System.out.println(barcodes.size());
    }

    @Test
    public void testRegex() {
        Pattern pattern = Pattern.compile(PDFUtils.regex);
        Matcher matcher = pattern.matcher(goodStaing);
        assertTrue(matcher.find());
    }

    @Test
    public void testRegexFullText() {
        List<String> barcodes = PDFUtils.getBarcodes(textExample);
        assertEquals(countGroups, barcodes.size());
    }

    @Test
    public void testRegexRealText() {
        List<String> barcodes = PDFUtils.getBarcodes(realExample);
        assertEquals(3, barcodes.size());
    }

    @Test
    public void testGetBarcodes() {
        List<Barcode> barcodes = PDFUtils.getBarcodesList(textExample);
        assertEquals(countGroups, barcodes.size());
    }
}
