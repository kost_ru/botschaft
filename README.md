# README #

The purpose of this project is to notify applicants about their visas status.

### How it works? ###

There is a website of german embassy in Moscow -- [German embassy](http://www.germania.diplo.de/Vertretung/russland/ru/02-mosk/1-visa/3-merkblaetter/nationale-visa/0-nationale-visa.html)
At this page there are tons of links to documents, templates etc.
Also there is a link "Обработанные заявления о выдаче визы/ Entschiedene Visumanträge [pdf, 172.97k]"
It is a pdf with table of people who was approved to get visa (barcode, necessary documents, deadline date).

This application will time by time check the page for actual pdf, parse pdf, save new barcodes into DB and notify applicants by e-mail.

### Which technologies does it use? ###

It is a ordinary JavaEE application with business logic in EJB and data layer in JPA.
By the way, technology stack:
1) Gradle -- to build EAR;
2) Apache PDF -- to parse PDF file;
3) Apache HTTP Client -- to download HTML and PDF files;
4) JPA 2.1 -- to work with DB;
5) EJB 3.1 -- to implement business logic;
6) JUnit 4.11 -- to test;
6) SLF4J & Logback -- to log into STDOUT, STDERRR and separate file.

### Architecture ###

It is consist of 4 submodule:
1) botschaft-ear -- gradle config to build EAR;
2) botschaft-ejb -- stateless session beans with logic;
3) model -- JPA entities;
4) parsing -- utils static classes for parsing HTML and PDF.

A few words about submodules.
1) botschaft-ejb has 
a) RequestTimerBean -- it uses timer service to run logic periodically;
b) BarcodesFetcherBean -- it's the "main" class. It uses utiliy classes from parsing-submodule in oder to get direct link to actual pdf, then parse it, save new into into DB and notify people.
c) BarcodeServiceBean, MailRecipientServiceBean is used to work with JPA (I didn't use here DAO pattern, because my task was too simple)


### What about enviroment? ###

I've test the app on the next machines:
1) MacOS X 10.9, Glassfish 4.1, PostgreSQL;
2) Windows 7, Weblogic 12c, Oracle RDBMS 11g.

### Results? ###

It works pretty well, i was notified by this app when my "Arbeitsplatzsuche"-visa was ready :)

# P.S. #
[linkedin](https://www.linkedin.com/profile/view?id=268469876&trk=spm_pic)
[facebook](https://www.facebook.com/constantin.pasko)
paskokv@gmail.com